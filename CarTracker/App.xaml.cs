﻿using CarTracker.Tools;
using MahApps.Metro;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace CarTracker
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Config.Load();

            Accent accent = ThemeManager.GetAccent(Config.Settings.AppSettings.Accent);
            AppTheme appTheme = ThemeManager.GetAppTheme(Config.Settings.AppSettings.Theme);

            ApplicationStyleChanger.ChangeApplicationStyle(accent, appTheme);

            base.OnStartup(e);
        }
    }
}
