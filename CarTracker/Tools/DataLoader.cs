﻿using CarTracker.Models;
using CarTracker.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CarTracker.Tools
{
    public class DataLoader
    {
        private readonly string filePath;

        public DataLoader(string filePath = Constants.VehicleDataFilePath)
        {
            this.filePath = filePath;
        }

        public IEnumerable<VehicleData> GetAll(Func<VehicleData, bool> filter = null)
        {
            var jsonFormat = JsonFormat<VehicleData>.Import(filePath);

            if (filter != null)
            {
                return jsonFormat.Data.Where(filter);
            }

            return jsonFormat.Data;
        }

        public void Save(VehicleData vehicleData)
        {
            var jsonFormat = JsonFormat<VehicleData>.Import(filePath);

            if (vehicleData.Id == 0)
            {
                vehicleData.Id = GetNextId();
            }
            else
            {
                int index = jsonFormat.Data.FindIndex(x => x.Id == vehicleData.Id);
                jsonFormat.Data.RemoveAt(index);
            }

            jsonFormat.Data.Add(vehicleData);

            JsonFormat<VehicleData>.Export(filePath, jsonFormat.Data);
        }

        public void Delete(VehicleData vehicleData)
        {
            var data = JsonFormat<VehicleData>.Import(filePath);

            int index = data.Data.IndexOf(vehicleData);
            data.Data.RemoveAt(index);

            JsonFormat<VehicleData>.Export(filePath, data.Data);
        }

        private int GetNextId()
        {
            int id = 1;
            var jsonFormat = JsonFormat<VehicleData>.Import(filePath);

            if (jsonFormat.Data.Count > 0)
            {
                VehicleData lastVehicleData = jsonFormat.Data[jsonFormat.Data.Count - 1];
                id = lastVehicleData.Id + 1;
            }

            return id;
        }
    }
}
