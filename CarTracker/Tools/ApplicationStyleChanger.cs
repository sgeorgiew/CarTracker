﻿using MahApps.Metro;
using System;
using System.Windows;

namespace CarTracker.Tools
{
    public static class ApplicationStyleChanger
    {
        public static void ChangeWindowStyle(Window window, Accent accent, AppTheme theme)
        {
            ThemeManager.ChangeAppStyle(window, accent, theme);
        }

        public static void ChangeApplicationStyle(Accent accent, AppTheme theme)
        {
            ThemeManager.ChangeAppStyle(Application.Current, accent, theme);
        }
    }
}
