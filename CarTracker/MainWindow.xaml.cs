﻿using CarTracker.Models;
using CarTracker.Tools;
using CarTracker.Views;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CarTracker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public static MainWindow Instance;
        public ObservableCollection<VehicleData> Data;

        private decimal totalSpentMoney;

        public MainWindow()
        {
            this.Data = new ObservableCollection<VehicleData>();
            this.totalSpentMoney = 0;

            InitializeComponent();

            Instance = this;

            Loaded += WindowLoaded;
            Closing += WindowClosing;
        }

        #region Private Methods
        private void LoadData()
        {
            DataLoader dataLoader = new DataLoader();

            var data = dataLoader.GetAll();

            foreach (var item in data)
            {
                Data.Add(item);
                totalSpentMoney += item.Price;
            }

            UpdateTotalSpent();
        }

        private void ViewData()
        {
            if (DataGrid.SelectedItems.Count <= 0)
            {
                return;
            }

            VehicleDataWindow vehicleDataWindow = new VehicleDataWindow(DataGrid.SelectedItem as VehicleData);
            vehicleDataWindow.ShowDialog();
        }

        private async Task DeleteDataAsync()
        {
            int selectedItemsCount = DataGrid.SelectedItems.Count;

            if (selectedItemsCount <= 0)
            {
                return;
            }

            MessageDialogResult dialogResult;
            if (selectedItemsCount > 1)
            {
                dialogResult = await this.ShowMessageAsync(
                            "Remove",
                            string.Format(
                                "You are about to remove {0} records. Are you sure?", selectedItemsCount),
                            MessageDialogStyle.AffirmativeAndNegative);
            }
            else
            {
                dialogResult = await this.ShowMessageAsync("Remove", "Are you sure?", MessageDialogStyle.AffirmativeAndNegative);
            }

            if (dialogResult == MessageDialogResult.Negative)
            {
                return;
            }

            DataLoader dataLoader = new DataLoader();
            var selectedItems = DataGrid.SelectedItems.Cast<VehicleData>().ToList();
            
            foreach (VehicleData item in selectedItems)
            {
                totalSpentMoney -= item.Price;
                dataLoader.Delete(item);
                Data.Remove(item);
            }

            UpdateTotalSpent();
        }
        #endregion

        #region Public Methods
        public void UpdateTotalSpent(decimal newPrice = -1, decimal oldPrice = -1)
        {
            if (newPrice != -1 && oldPrice != -1)
            {
                totalSpentMoney = (totalSpentMoney - oldPrice) + newPrice;
            }
            
            TotalSpentLabel.Content = string.Format(System.Globalization.CultureInfo.CurrentCulture, "Total Spent: {0:C}", totalSpentMoney);
        }
        #endregion

        #region Events
        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            this.DataGrid.ItemsSource = Data;
            LoadData();
        }

        private void WindowClosing(object sender, CancelEventArgs e)
        {
            Config.Settings.AppSettings.Width = this.Width;
            Config.Settings.AppSettings.Height = this.Height;

            Config.Save();
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settingsWindow = new SettingsWindow();
            settingsWindow.ShowDialog();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            VehicleDataWindow vehicleDataWindow = new VehicleDataWindow();
            vehicleDataWindow.ShowDialog();
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void DataGrid_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ViewData();
        }

        private void CmEdit_Click(object sender, RoutedEventArgs e)
        {
            ViewData();
        }

        private async void CmDelete_Click(object sender, RoutedEventArgs e)
        {
            await DeleteDataAsync();
        }
        #endregion
    }
}
