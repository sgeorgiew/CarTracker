﻿using CarTracker.Helpers;
using CarTracker.Models;
using MahApps.Metro;
using Newtonsoft.Json;
using System;
using System.IO;

namespace CarTracker
{
    public class Config
    {
        public ApplicationSettings AppSettings { get; set; }

        public static Config Settings;

        static Config()
        {
            if (!File.Exists(Constants.ConfigFileName))
            {
                Settings = new Config
                {
                    AppSettings = new ApplicationSettings
                    {
                        Accent = ThemeManager.GetAccent("Blue").Name,
                        Theme = ThemeManager.GetAppTheme("BaseDark").Name,
                        Width = 825,
                        Height = 500
                    }
                };

                Save();
            }
        }

        public static void Save()
        {
            using (StreamWriter sw = new StreamWriter(Constants.ConfigFileName))
            {
                sw.WriteLine(JsonConvert.SerializeObject(Settings));
            }
        }

        public static void Load()
        {
            using (StreamReader sr = new StreamReader(Constants.ConfigFileName))
            {
                Settings = JsonConvert.DeserializeObject<Config>(sr.ReadToEnd());
            }
        }
    }
}
