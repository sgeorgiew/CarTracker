﻿using CarTracker.Models;
using CarTracker.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CarTracker.Views
{
    /// <summary>
    /// Interaction logic for VehicleDataWindow.xaml
    /// </summary>
    public partial class VehicleDataWindow
    {
        private VehicleData vehicleData;
        private bool isUpdate;

        public VehicleDataWindow(VehicleData vehicleData = null)
        {
            InitializeComponent();

            this.vehicleData = vehicleData;

            if (vehicleData != null)
            {
                DatePicker.SelectedDate = vehicleData.Date;
                MileageTextBox.Text = vehicleData.Mileage.ToString();
                PriceTextBox.Text = vehicleData.Price.ToString();

                isUpdate = true;

                TextRange textRange = new TextRange(DescriptionRichTextBox.Document.ContentStart, DescriptionRichTextBox.Document.ContentEnd)
                {
                    Text = vehicleData.Description
                };
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (!DatePicker.SelectedDate.HasValue)
            {
                ResultLabel.Content = "Please select a date!";
                return;
            }

            if (!int.TryParse(MileageTextBox.Text, out int mileage))
            {
                ResultLabel.Content = "Please enter a valid mileage!";
                return;
            }

            if (!decimal.TryParse(PriceTextBox.Text, out decimal price))
            {
                ResultLabel.Content = "Please enter a valid price!";
                return;
            }

            DateTime date = DatePicker.SelectedDate.Value;

            TextRange textRange = new TextRange(DescriptionRichTextBox.Document.ContentStart, DescriptionRichTextBox.Document.ContentEnd);
            string description = textRange.Text.Trim('\r', '\n', ' ');

            try
            {
                VehicleData newVehicleData = vehicleData ?? new VehicleData();

                decimal oldPrice = newVehicleData.Price;

                newVehicleData.Date = date;
                newVehicleData.Mileage = mileage;
                newVehicleData.Price = price;
                newVehicleData.Description = description;

                DataLoader dataLoader = new DataLoader();
                dataLoader.Save(newVehicleData);

                if (!isUpdate)
                {
                    MainWindow.Instance.Data.Add(newVehicleData);

                    MileageTextBox.Text = String.Empty;
                    PriceTextBox.Text = String.Empty;
                    textRange.Text = String.Empty;
                }

                ResultLabel.Content = "Successfuly saved!";

                MainWindow.Instance.UpdateTotalSpent(newVehicleData.Price, oldPrice);
            }
            catch
            {
                ResultLabel.Content = "Something went wrong!";
            }
        }
    }
}
