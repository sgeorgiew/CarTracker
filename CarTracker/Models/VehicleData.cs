﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CarTracker.Models
{
    public class VehicleData : INotifyPropertyChanged, IModel
    {
        public int Id { get; set; }

        private string description;
        public string Description
        {
            get { return description; }
            set { SetAndNotify(ref description, value, nameof(Description)); }
        }

        private int mileage;
        public int Mileage
        {
            get { return mileage; }
            set { SetAndNotify(ref mileage, value, nameof(Mileage)); }
        }

        private decimal price;
        public decimal Price
        {
            get { return price; }
            set { SetAndNotify(ref price, value, nameof(Price)); }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { SetAndNotify(ref date, value, nameof(Date)); }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void SetAndNotify<T>(ref T field, T value, string propertyName)
        {
            if (!EqualityComparer<T>.Default.Equals(field, value))
            {
                field = value;
                RaisePropertyChanged(propertyName);
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override bool Equals(object obj)
        {
            VehicleData data = obj as VehicleData;

            if (data == null)
            {
                return false;
            }

            return this.Id == data.Id;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
